import logging
import os
import random
import json

from urllib.request import FancyURLopener
from html.parser import HTMLParser
from queue import Queue
from threading import Thread
from telegram import Bot, TelegramError, ParseMode
from telegram.ext import Dispatcher, CommandHandler, MessageHandler, Updater, Filters
from emoji import emojize

from constants import Purity

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

TOKEN = os.environ['TOKEN']
GIPHY_API_KEY = os.environ['GIPHY_API_KEY']
SESSION_COOKIE = os.environ['SESSION_COOKIE']

# emoji
blush = emojize(':blush:', use_aliases=True)
kiss = emojize(':kiss:', use_aliases=True)
kissing_heart = emojize(':kissing_heart:', use_aliases=True)
kissing_closed_eyes = emojize(':kissing_closed_eyes:', use_aliases=True)
lips = emojize(':lips:', use_aliases=True)
kissing_cat = emojize(':kissing_cat:', use_aliases=True)
couple_kiss = emojize(':couplekiss:', use_aliases=True)

# kiss emoji list
kisses = [kiss, kissing_heart, kissing_closed_eyes,
          lips, kissing_cat, couple_kiss]

hello_message = """Приветик %s
Введи /help и я тебе покажу список комманд""" % blush

help_message = """Список комманд, которые я могу выполнить:
/anime - случайная аниме-картинка
/anime_gif - случайная аниме-гифка
/anime_nsfw - случайная аниме-картинка (без возврастных ограничений)
/cat - случайная картинка кошки или кота
/dog - случайная картинка собаки
/ecchi - случайная аниме-картинка с этти
/hentai - случайная аниме-картинка с хентаем или этти
/furry - случайная картинка с фурри
/furry_nsfw - случайная картинка с фурри (без возврастных ограничений)
/neko - случайная картинка с неко
/neko_nsfw - случайная картинка с неко (без возврастных ограничений)
/chmok - чмокнуть %s
""" % kiss

# wallhaven templates
search_template = "https://alpha.wallhaven.cc/search?q={keyword}&categories=010&purity={purity}&page=1&resolutions=1920x1080&sorting=random&order=desc"
wallpaper_url_template = "https://alpha.wallhaven.cc/wallpaper/{0}"
full_image_template = "https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-{0}"
full_image_template_jpg = "{0}.jpg"
full_image_template_png = "{0}.png"

# giphy templates
random_anime_gif_url = "https://api.giphy.com/v1/gifs/random?api_key={0}&tag=anime".format(
    GIPHY_API_KEY)

# command functions dictionary
commands = {}


class URLopener(FancyURLopener):
    version = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.168 Safari/537.36"


class WallhavenParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.image_ids = []

    def handle_starttag(self, tag, attributes):
        if tag != 'figure':
            return

        for name, value in attributes:
            if name == 'data-wallpaper-id':
                self.image_ids.append(value)

# Get random image url and id tuple (url, id)


def get_image(keyword, purity):
    settings = {'keyword': keyword, 'purity': "{:03d}".format(purity)}
    url = search_template.format(**settings)
    opener = URLopener()
    opener.addheader('Cookie', SESSION_COOKIE)
    logger.warning(url)
    response = opener.open(url)
    html = response.read().decode('utf-8')
    parser = WallhavenParser()
    parser.feed(html)
    parser.close()
    image_id = random.choice(parser.image_ids)
    full_image_url = full_image_template.format(image_id)
    return (full_image_url, image_id)


def send_photo(image_tuple, bot, update):
    (image, id) = image_tuple
    jpg_image = full_image_template_jpg.format(image)
    png_image = full_image_template_png.format(image)
    wallpaper_url = wallpaper_url_template.format(id)

    try:
        logger.warning(jpg_image)
        bot.send_photo(chat_id=update.message.chat_id, photo=jpg_image)
    except TelegramError:
        logger.warning(png_image)
        bot.send_photo(chat_id=update.message.chat_id, photo=png_image)

    bot.send_message(chat_id=update.message.chat_id,
                     text="[Полная версия]({0})".format(wallpaper_url),
                     disable_web_page_preview=True,
                     parse_mode=ParseMode.MARKDOWN)


def get_gif():
    opener = URLopener()
    logger.warning(random_anime_gif_url)
    response = opener.open(random_anime_gif_url)
    gif_obj = json.loads(response.read().decode('UTF-8'))
    return (gif_obj['data']['type'], gif_obj['data']['images']['original']['url'])

# Command handlers


def start(bot, update):
    update.message.reply_text(hello_message)


def help(bot, update):
    update.message.reply_text(help_message)


def anime(bot, update):
    image = get_image("", Purity.SFW)
    send_photo(image, bot, update)


def anime_nsfw(bot, update):
    image = get_image("", Purity.SCETCHY | Purity.NSFW)
    send_photo(image, bot, update)


def ecchi(bot, update):
    image = get_image("ecchi", Purity.SFW | Purity.SCETCHY | Purity.NSFW)
    send_photo(image, bot, update)


def hentai(bot, update):
    if update.message.chat.type != 'private':
        update.message.reply_text("Такое только в личку {0}".format(blush))
        return

    image = get_image("hentai", Purity.NSFW)
    send_photo(image, bot, update)


def furry(bot, update):
    image = get_image("furry", Purity.SFW)
    send_photo(image, bot, update)


def furry_nsfw(bot, update):
    image = get_image("furry", Purity.SCETCHY | Purity.NSFW)
    send_photo(image, bot, update)


def neko(bot, update):
    image = get_image("neko", Purity.SFW)
    send_photo(image, bot, update)


def neko_nsfw(bot, update):
    image = get_image("neko", Purity.SCETCHY | Purity.NSFW)
    send_photo(image, bot, update)


def chmok(bot, update):
    update.message.reply_text(random.choice(kisses))


def anime_gif(bot, update):
    (type, gif) = get_gif()

    logger.warning("{0}: {1}".format(type, gif))

    if type != 'gif':
        update.message.reply_text(gif)
    else:
        bot.send_document(chat_id=update.message.chat_id, document=gif)


def dog(bot, update):
    image = get_image("dog", Purity.SFW)
    send_photo(image, bot, update)


def cat(bot, update):
    image = get_image("cat", Purity.SFW)
    send_photo(image, bot, update)


def other(bot, update):
    logger.warning(update.message.text)
    func = commands.get(update.message.text)

    if func:
        func(bot, update)


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


commands['start'] = start
commands['help'] = help
commands['anime'] = anime
commands['anime_nsfw'] = anime_nsfw
commands['ecchi'] = ecchi
commands['cat'] = cat
commands['dog'] = dog
commands['anime_gif'] = anime_gif
commands['hentai'] = hentai
commands['furry'] = furry
commands['furry_nsfw'] = furry_nsfw
commands['neko'] = neko
commands['neko_nsfw'] = neko_nsfw
commands['chmok'] = chmok


def setup(webhook_url=None):
    """If webhook_url is not passed, run with long-polling."""
    logging.basicConfig(level=logging.WARNING)
    if webhook_url:
        bot = Bot(TOKEN)
        update_queue = Queue()
        dp = Dispatcher(bot, update_queue)
    else:
        updater = Updater(TOKEN)
        bot = updater.bot
        dp = updater.dispatcher
        dp.add_handler(CommandHandler("start", start))
        dp.add_handler(CommandHandler("help", help))
        dp.add_handler(CommandHandler("anime", anime))
        dp.add_handler(CommandHandler("anime_nsfw", anime_nsfw))
        dp.add_handler(CommandHandler("ecchi", ecchi))
        dp.add_handler(CommandHandler("hentai", hentai))
        dp.add_handler(CommandHandler("furry", furry))
        dp.add_handler(CommandHandler("furry_nsfw", furry_nsfw))
        dp.add_handler(CommandHandler("neko", neko))
        dp.add_handler(CommandHandler("neko_nsfw", neko_nsfw))
        dp.add_handler(CommandHandler("chmok", chmok))
        dp.add_handler(CommandHandler("kiss", chmok))
        dp.add_handler(CommandHandler("anime_gif", anime_gif))
        dp.add_handler(CommandHandler("dog", dog))
        dp.add_handler(CommandHandler("cat", cat))

        # on noncommand i.e message - try parse general commands and map them
        dp.add_handler(MessageHandler(Filters.command, other))

        # log all errors
        dp.add_error_handler(error)
    # Add your handlers here
    if webhook_url:
        bot.set_webhook(webhook_url=webhook_url)
        thread = Thread(target=dp.start, name='dispatcher')
        thread.start()
        return update_queue, bot
    else:
        bot.set_webhook()  # Delete webhook
        updater.start_polling()
        updater.idle()


if __name__ == '__main__':
    setup()
