FROM python:3.6-alpine as base

COPY app.py /
COPY constants.py /
COPY requirements.txt /

RUN apk add --no-cache gcc musl-dev libffi-dev openssl-dev
RUN pip install -r /requirements.txt

CMD [ "python", "./app.py" ]
